\documentclass{tufte-handout} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{geometry} % to change the page dimensions

\usepackage{graphicx} 
\usepackage{amsmath,amsthm,url}
\newtheorem{lemma}{Lemma}
%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim

\usepackage[draft,inline,nomargin,index]{fixme}
\fxsetup{theme=colorsig,mode=multiuser,inlineface=\sffamily,envface=\itshape}
\FXRegisterAuthor{su}{asu}{Suresh}
\FXRegisterAuthor{ro}{aro}{Robert}


\newcommand{\determin}[1]{\mathcal{D}\left(#1\right)}
\newcommand{\equ}{\mathcal{E\!Q}}
\newcommand{\disjoint}{\mathcal{DISJ}}
\newcommand{\mP}{\mathcal{P}}
%%% END Article customizations

%%% The "real" document content comes below...

\title{CS 7931: Lecture 1}
\author{Robert Christensen}
%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 

\begin{document}
\maketitle

\section{What is Communication Complexity about?}

A model of how to understand how programs interact to solve a problem.
The model was proposed in the late 70s by Andy Yao, which extracts out only the
communications and ignores internal computation \cite{Yao:1979:CQR:800135.804414}.

%\sunote{Add a citation to the original paper by Andy Yao %\url{http://en.wikipedia.org/wiki/Communication_complexity\#cite_note-yao1979-1}}

\begin{marginfigure}
\centering
\includegraphics{figs/AliceBob1}
\end{marginfigure}

Given that Alice has the data $x$ and Bob has the data $y$, how
much communication is needed in order to compute the function
$f(x,y)$ ?   The function could be anything involving communication,
such as string comparison, set comparison, numerical computation
(mean, median, $\ldots$)

We only care about the communication between Alice and Bob. The time
for internal computation within Alice and Bob is ignored in this model.
For example, in this model Alice might even be allowed to solve the halting problem before
responding to Bob.  All that we consider while using this model is
the communication which takes place between Alice and Bob.

One of the primary uses of this model is to prove lower bounds on algorithms.
This method can be used to prove lower bounds on an algorithm in a different
more complicated model.  This works because if we prove a lower bound for
communication cost, the algorithm can not be any faster than the communication
cost of the algorithm.

This model is especially useful for studying streaming algorithms and distributed
systems where communication between machines or processors is much more
significant than processing local data.

% example of looking up Alice's ith bit was given here but ommited from the notes
% because the future information is deamed to be more interesting for the seminar

\section{What Communication Looks Like in This Model}

\subsection{Computation for Computing $\equ$}

Lets assume that $x$ and $y$ are $n$-bit strings and $f(x,y) = \equ$: is $x=y$? (are
the strings identical)?  How can we do this?

The simple solution is for Alice to send all her data to
Bob.  Bob can check if $x=y$ locally
and responds to Alice \verb^true^ or \verb^false^.  The
communication cost is determined by
the length of $x$ and the single bit returned to indicate
\verb^true^ or \verb^false^

\begin{equation*}
\determin{\equ} \le |x| + 1 = n+1
\end{equation*}

\subsection{Computation of disjoint set}

Let the $n$-bit strings $x, y$ represent the characteristic vectors of sets
$S_x$ and $S_y \subset [n]$ (i.e $i \in S_X \Leftrightarrow x_i = 1$). 
We define the function $\disjoint(x, y) = 1_{S_x \cap S_y = \emptyset}$. 
A trivial solution to this problem uses the same process from computing
$\equ$: Have Alice send $x$ to Bob, Bob computes the function and returns
\verb^true^ or \verb^false^.  The deterministic complexity for $\disjoint{}$
is the same as for $\equ$.

\begin{equation*}
\determin{\disjoint} \le |x| + 1
\end{equation*}

\subsection{Protocol Trees}

We can describe a communication protocol abstractly by a \emph{protocol tree}. 

\begin{marginfigure}
\centering
\includegraphics[width=1.5in]{figs/FunctionTree}
\end{marginfigure}

\begin{itemize}
\item A non-leaf node is associated with either Alice or Bob. Attached to this
  node is a function that takes the corresponding input as well as all
  communication thus far, and outputs $b \in \{0, 1\}$. The result
of the function indicates which branch of the
tree will be pursued next.
\item Leaf nodes are labeled with the output of the protocol.
\end{itemize}

% without loss of generality, we do not have to assume the
% individual associated with a node flip-flops.  If we wanted
% to remove that restriction we would just have a null
% function at some nodes which would return the control
% to the other so they can continue to send more data.

% there is a slight difference between somebody knowing
% the answer and everybody knowing the anwer, but this
% difference is not significant for our purposes.

The protocol associated with a particular input $(x,y)$ corresponds to a
root-to-leaf path in the protocol tree. Therefore, the length of this path
equals the total communication between Alice and Bob. The height of the protocol
tree represents the worst-case complexity (over all inputs). 
The goal is of course to design a protocol tree whose height is minimized, or
show that any protocol tree must have at least a certain height.

\subsection{Some Examples}
\label{sec:some-examples}


% recording ~28:00
\paragraph{Joint Mean}

Alice and Bob each have a collection of integers drawn from $[1\dots n]$, and
the goal is to compute the mean of the union of the two sets. 
Note that Alice cannot just send her average to Bob because the number might not
be representable with a finite number of bits (such as $\frac{1}{7} = 0.\overline{142857}$).
However, Alice can send the average as a ratio of two numbers, the sum and the
count. This information is sufficient for Bob to compute the joint average and
send back a corresponding pair. 

\begin{marginfigure}
\centering
\includegraphics[width=1.5in]{figs/mean}
\end{marginfigure}

The maximum size of $\sum x$ is $n^2$.  In order to describe a value of
size $n^2$ we need $2 \log n$ bits.  Thus, the communication complexity
to solve joint mean is

\begin{equation*}
\determin{\text{Joint Mean}} = O(\log n)
\end{equation*}
%\sunote{Complete the argument ? }

This is because $\sum x$ must be communicated, which takes $O(\log n)$
bits to describe.

% recording ~30:30
\paragraph{Joint Median}

\begin{align*}
A &\subseteq \{1 \ldots n\} \\
B &\subseteq \{1 \ldots n\} 
\end{align*}

The values of $A$ and $B$ might overlap.

Both start out thinking the median could be anywhere
in their range. Denote this range of uncertainty as $I$, and let $m_I$ be its
midpoint. Initially, $I = [1\dots n]$  Alice sends
$\sum_{k=0}^{|A|} \left( A_k > m_I \right)$
(The number of elements in $A$ which are larger than
some interval $I$).  In the beginning $m_I=\frac{n}{2}$
Bob excludes the portion of the data which we know
the median can not be a part of.  Bob will update
the interval $I$ by reporting if the median is
on the `left' or the `right' side of Alice's data.
Eventually both will agree that the median must
lay within a specific interval.

For each round Alice must send the number of elements
larger than the interval $I$ to Bob which takes $O(\log n)$
bits to communicate.  Bob returns with a single bit, taking
$1$ bit to communicate.  Since we are doing a binary search,
$O(\log n)$ rounds will occur before the answer is
computed.  The complexity of this is

% recording ~35:00-39:20
\begin{equation*}
\determin{\text{med}} \le \log^2(n)
\end{equation*}

%\sunote{why?}

Using some tricks.  We can send less information
in future rounds because the range of where the median
could possible by is shrinking.  Using these tricks
the complexity shrinks to


\begin{equation*}
\determin{\text{med}} \le \log(n)
\end{equation*}

This is given as a problem for readers to solve.\footnote{We discuss the
  solution in our mailing list.}

\section{Proving Lower Bounds in Communication Cost}

We define a combinatorial rectangle with the following definition:

\begin{align*}
R &= A \times B \\
  & A \subseteq X \\
  & B \subseteq Y
\end{align*}

\begin{marginfigure}
\centering
\includegraphics[width=2in]{figs/rectangle}
\end{marginfigure}

Another, more useful, definition of a combinatorial rectangle

\begin{equation*}
R \subseteq x \times y~\text{is a rectangle iff}
\end{equation*}
\vspace{-1cm}
\begin{align*}
(x_1, y_1) \in R, (x_2, y_2) &\in R \Rightarrow \\
(x_1, y_2) &\in R \\
(x_2, y_1) &\in R
\end{align*}

In other words, if you get the diagonal corners we also get the
anti-diagonal corners.

We now define an interesting property of the inputs associated with a leaf of a
protocol tree. For any leaf $i$, let $R_i$ denote the subset of $X \times Y$
that causes the protocol to end at this leaf. Note that the sets $R_1, R_2,
\dots, R_L$ is a partition of $X \times Y$. 

% recording ~46:00
\begin{lemma}
  For any leaf $i$, $R_i$ is a combinatorial rectangle. 
\end{lemma}

%Alice only knows $x$ and Bob only knows $y$.  If $(x_1, y_1)$ results
%in the same tree traversal as $(x_2, y_2)$ this means that the communication
%transcript it identical between the two inputs.  If we interchange $x_1$
%and $x_2$ Alice will still have the same communication transcript even though
%the input is different.  Bob remains ignorant.

Alice only knows $x$ and Bob only knows $y$.  If $(x_1, y_1)$ results
in the same tree traversal as $(x_2, y_2)$ this means that the communication
transcript it identical between the two inputs.  If we interchange $y_1$
and $y_2$, at the first step.  If the first step is Alice's step, she will make
the same decision (because she has the same input, $x_1$).  Lets say the next
step is Bob.  Bob is looking at his input $y_2$, we know when we has $(x_2, y_2)$
as input the path that we took to get to our current location in the decision tree
the same.  Since Bob is at the same location in the tree and Alice has communicated
the same information as input for the node, Bob will make the same decision at
the node, so the next step is the same.  This pattern will continue until we reach
the leaf node, so if $(x_1, y_1)$ and $(x_2, y_2)$ arrive in the same combinatorial
rectangle, the input $(x_1, y_2)$ and $(x_2, y_1)$ will also arrive in the same
combinatorial rectangle.

%\sunote{This is a plausible sketch of the argument, but it can certainly be made
%  a little more precise.}

Having the same solution does not always mean you reach the same
leaf, but if you have the same leaf you will have the same answer.

For example, the following is the decision matrix for equality.

\begin{marginfigure}
\centering
\includegraphics[width=2in]{figs/equal_rectangle}
\end{marginfigure}

There is nowhere we can make a rectangle to cover more of the `1' solutions since
there is no configuration where the rectangle will encompass no `0's.

The following figure shown another example of a configuration which does not
work.

\begin{marginfigure}
\centering
\includegraphics[width=2in]{figs/example}
\end{marginfigure}

Because we can not create any larger rectangles in the solution:

\begin{equation*}
\ell \ge L = 2^n ~~~~\text{for equality problem}
\end{equation*}

This is called a `fooling set' argument.

\newpage

\bibliographystyle{plain}
\bibliography{L1}

\end{document}
