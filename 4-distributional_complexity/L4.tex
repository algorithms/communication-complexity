\documentclass{tufte-handout} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{geometry} % to change the page dimensions

\usepackage{graphicx} 
\usepackage{amsmath,amsthm,url}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim

\usepackage[draft,inline,nomargin,index]{fixme}
\fxsetup{theme=colorsig,mode=multiuser,inlineface=\sffamily,envface=\itshape}
\FXRegisterAuthor{su}{asu}{Suresh}
\FXRegisterAuthor{ro}{aro}{Robert}


\newcommand{\determin}[1]{\ensuremath{\mathcal{D}\left(#1\right)}}
\newcommand{\randR}[1]{\ensuremath{\mathcal{R}\!\left(#1\right)}}
\newcommand{\equ}{\ensuremath{\mathcal{E\!Q}}}
\newcommand{\gtfunc}{\ensuremath{\mathcal{GT}}}
\newcommand{\disjoint}{\ensuremath{\mathcal{DISJ}}}
\newcommand{\mP}{\ensuremath{\mathcal{P}}}
\newcommand{\nexists}{\ensuremath{\not{\exists}}}
%%% END Article customizations

%%% The "real" document content comes below...

\title{CS 7931: Lecture 4}
\author{Robert Christensen}
%\date{September 17, 2014}

\begin{document}
\maketitle

\section{Why Randomized Algorithms Perform Better than Deterministic}

When we think of the algorithms we used to calculate the answer for \equ{},
the deterministic complexity was $\determin{\equ}=O(n)$ but the randomized algorithm
was significantly less, with a complexity of $\randR{\equ}=O(n)$.  How are the
randomized algorithms able to avoid the communication cost which the deterministic
algorithms must take?

For a deterministic algorithms, no error can be allowed.  Which means that in
the solution matrix, all rectangles \emph{must} be monochromatic for the
deterministic algorithm.  In other words, the leaf level of the communication
tree must all be the same value.  To show a deterministic algorithm fails,
it is enough to show that it fails for one set of inputs.

\begin{marginfigure}
\centering
\includegraphics{figs/ones_zeros_decision_tree}
\caption{For a decision tree such as the one above, a deterministic version of an algorithm  must have a unique leaf which will lead to the  result.  Since randomized algorithms allow for some error, all the items could reside in a single leaf.}
\end{marginfigure}

Randomized algorithms can allow for mistakes, so the rectangles in the
solution matrix do not have to be monochromatic, just mostly monochromatic.
By allowing for some error, the hight tree can be significantly compressed.

For a deterministic algorithm, the communication complexity to find a
solution is

\begin{equation*}
\min_A \max_x T_A(x)
\end{equation*}

Where $A$ is the set of all algorithms which would produce the correct solution
given any input $x$.  $T_A(x)$ is the time for the algorithm $A$ to find the solution
given input $x$.  The following constraints must be accommodated.

\begin{equation*}
\exists x~s.t.~\forall A~T_A(x) \ge f(|x|)
\end{equation*}

This must be true for all deterministic algorithms.  If it fails under any condition,
the deterministic algorithm is not correct.

Randomized algorithms have more forgiveness.  The communication complexity
for a randomized algorithm is

\begin{equation*}
\min_A \max_{\log{(1-\epsilon)n}} T_A(x)
\end{equation*}

Because randomized algorithms have increased forgiveness,  they can have larger leaf
level blocks making the tree have less depth.

\section{Distributional Complexity}

Given a deterministic algorithm $A$ and some distribution $\mu : \rightarrow \{0,1\}$ over the inputs,
we will compute the cost of the algorithm where the input to the algorithm is generated
using $\mu$.  We will only consider algorithms where $\text{Pr}_{\mu} \left( A(x)~\text{is wrong} \right) \le \epsilon$.
In other words, we do not consider the possible algorithms which
 produce an error greater than $\epsilon$.

With this, the deterministic complexity is

\begin{equation*}
\mathcal{D}_\epsilon\left(f\right) = \min_{A} \max_{\mu} T(A)
\end{equation*}

Now we want to reason about the comparison between the complicity of
deterministic algorithms with random inputs (which can make some
small number of mistakes) and randomized algorithms.

\begin{equation*}
\mathcal{D}_\epsilon\left(f\right) = \mathcal{R}_\epsilon\left(f\right)
\end{equation*}

This means that when determining lower bounds for algorithms, the source
of the coin tosses (whether internal to the algorithm or external to the algorithm)
does not matter.  The communication complexity is the same.

The deterministic algorithm itself no longer uses a source of random data.  Whoever
is providing the data can produce data which involves randomness.

To further clarify, the deterministic algorithm can make some mistakes, but given the
same input the same output will always be returned even though the answer could
potentially be incorrect.  The deterministic algorithm has no internal source
of randomness.  The randomized algorithm does have an internal source of randomness,
so given the same input, the algorithm may produce a different result with some
probability.

The proof goes as follows.

Fix a function $f:~x\rightarrow\{0,1\}$ and $\mathcal{R}_\epsilon(f)$ which use $d$
bits.  We create a matrix where the rows are input algorithms which use $d$ bits
and the columns are the inputs to the algorithm.  We define two matrices $K_0$ and $K_1$.

\begin{align*}
K_0 &= \begin{cases}
1 & \text{if $A_j(x_i)=1$} \\
0 & \text{otherwise}
\end{cases} \\
K_1 &= \begin{cases}
1 & \text{if $A_j(x_i)=0$} \\
0 & \text{otherwise}
\end{cases} \\
\end{align*}

You can think of the matrix as tracking the mistakes being made by the algorithm.

The randomized complexity of the function $f$ can be written out using a linear program.

\begin{align*}
\mathcal{R}_\epsilon(f) &= \min_p \epsilon \\ 
                        & \text{s.t.}~K_0 p \le \in \hat{1}~\text{and}~K_1 p \le \in \hat{1}
\end{align*}

Where $p$ is a probability distributional.

Since this is a linear algorithm, there exists a duel.  We stack the matrix $K_1$ and $K_0$
and we want to maximize $\nu$ such that $K^\top \lambda \ge \nu$ where $\lambda$ is a distribution
function.

WE have gone from averaging over the columns to averaging over the rows.  If we remember what
the columns and rows represent, we have gone from reasoning about the algorithms to reasoning
about the inputs to the algorithms.  Because we can make this switch it proves 
$\mathcal{D}_\epsilon\left(f\right) = \mathcal{R}_\epsilon\left(f\right)$.  This is useful
because reasoning about deterministic algorithms is less complicated then reasoning about
randomized algorithms.  This is not only useful for communication complexity.  It is also
useful for reasoning about the cost of any limited resource such as space complexity
or processing time.

\subsection{Reasoning about \gtfunc}

With random inputs to the function \gtfunc, what is the lower complexity bound?  Since the 
input is random, we can use the following procedure to get an answer which will be correct
some probability of the time.  Alice sends the most significant bit $A_0$ to Bob.  Bob compares
$A_0$ with $B_0$.  If they differ, we know which one is larger and can correctly return
an answer.  If $A_0 \neq B_0$, we don't know who is larger, so Bob will report true.  Since
the input is random, reporting true always will be correct with $p=0.5$.  Thus, we will
be correct with $p=1/4$ by using this algorithm.  Since we only needed two bits to get
the answer correct with $p=1/4$,

\begin{equation*}
\mathcal{D}_{\frac{1}{4}}^\text{uniform} \left( \gtfunc \right) \le 2~\text{bits}
\end{equation*}

\subsection{Reasoning about \disjoint}

We know that if a collision exists, they are not disjoint.  The protocol
will be as follows.  Alice sends the first $\sqrt{n}$ bits to Bob.  Bob checks
if any collisions exist in the segment from Alice.  If Bob detects a collision he reports
they are not disjoint, otherwise he reports they are disjoint.

By the birthday paradox, if a collision exists with $p>0.5$ it will be found within the
first $\sqrt{n}$ elements, so the complexity is

\begin{equation*}
\mathcal{D}_{c}^{\text{uniform}} \left( \disjoint \right) = \sqrt{n}
\end{equation*}

where $c$ is some constant.

%\section{Randomized Product Distribution}
%
%We use the notation $\mathcal{R}^{[]}$ is used for randomized product distributions.
%Currently, distributions are determined by $\mu_1(X) \times \mu_2(Y)$.  A product distributions
%is defined with $\mu(X,Y) = (\mu_1(x))(\mu_2(y))$

\section{Discrepancy}

We now

When we are creating deterministic algorithms which allow for error, we want the rectangles in
the solution matrix to have high discrepancy.  We can think of discrepancy as a comparison of 
the ratio of $1$'s to $0$'s.  Low discrepancy indicates the number of $1$'s and $0$'s are almost
equal.  A low discrepancy rectangle is very confused (inside a rectangle with low discrepancy
the error will be large).

We want to have this measure so we can say that we can not make the rectangles any larger 
(thus making the algorithms faster) without causing too many errors.  The measure
which we are using to indicate the highest discrepancy allowed is

\begin{equation*}
\mathcal{D}_{1/2-\epsilon}^{\mu} \ge \log{\frac{2 \epsilon}{\text{Disc}_\mu(f)}}
\end{equation*}

\end{document}
