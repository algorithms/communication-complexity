\documentclass{tufte-handout} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{geometry} % to change the page dimensions

\usepackage{graphicx} 
\usepackage{amsmath,amsthm,url}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim

\usepackage[draft,inline,nomargin,index]{fixme}
\fxsetup{theme=colorsig,mode=multiuser,inlineface=\sffamily,envface=\itshape}
\FXRegisterAuthor{su}{asu}{Suresh}
\FXRegisterAuthor{ro}{aro}{Robert}


\newcommand{\determin}[1]{\ensuremath{\mathcal{D}\left(#1\right)}}
\newcommand{\randR}[1]{\ensuremath{\mathcal{R}\!\left(#1\right)}}
\newcommand{\equ}{\ensuremath{\mathcal{E\!Q}}}
\newcommand{\gtfunc}{\ensuremath{\mathcal{GT}}}
\newcommand{\disjoint}{\ensuremath{\mathcal{DISJ}}}
\newcommand{\mP}{\ensuremath{\mathcal{P}}}
\newcommand{\nexists}{\ensuremath{\not{\exists}}}
\newcommand{\disc}{\ensuremath{\text{\em Disc}_\mu}}
%\newcommand{\Pr}{\ensuremath{\text{Pr}}}
%%% END Article customizations

%%% The "real" document content comes below...

\title{CS 7931: Lecture 9}
\author{Robert Christensen}

\begin{document}
\maketitle

\section{Asymmetric Communication Complexity}

For asymmetric communication complexity we consider the standoffs of information
sent from Alice to Bob or from Bob to Alice (rather then all communication
between the parties treated equal, as is more common of communication
complexity).  Also, the size of the input is not the same.  The size of
Alice's input has much fewer bits of input then Bob\cite{Miltersen}.

\subsection{Membership problem}

Alice has $x \in U = \{0, \ldots, N-1 \}$.  Bob has $y \subseteq U$ with
size at most $N$.  We want to report true if $x \in y$ and false otherwise.

One obvious solution to this problem is for Alice to send $x$ to Bob.  Bob
checks if $x \in y$ and reports the answer.  Even though this is the naive solution,
it is optimal.

\subsection{Cell probe problem}

for a static data structure, we have $Q$ possible queries and $D$ possible data.
We want to compute the function $f: Q \times D \rightarrow A$.  We also have
the following parameters for the model.

\begin{description}
\item[$s$:] the total number of cells.
\item[$b$:] the size of each cell ($b$ is about $O(\log{|Q|})$.
\item[$t$:] the number of cells which need to be accessed.
\end{description}

$s$ describes the size of the data structure.
$t$ describes the query time.
$b$ is defined as part of the model.

A common problem solved in this model is the dictionary problem.
$D$ contains the subset $y \subset \{0, \ldots, N-1 \}$.  Also
$Q$ is the set $\{0, \ldots, N-1\}$.  Alice has the data of $Q$ as
input and Bob has $D$ as input.  $f(x,y)=1$ if and only if $x\in y$.
Basically, the query will return true if the value is part of the
dictionary.

\begin{lemma}
  \label{lem:bounds}
  If there is a solution with parameters $s$, $b$, $t$ than we will
  have a communication complexity of 
  [$2t$, $\lceil \log{s} \rceil$, $b$]$^A$. Where the superscript $A$
  signals the first to communicate, $2t$ describes the number of rounds,
  $2t$ rounds, $\lceil \log{s} \rceil$ bits sent by Alice, $b$ bits sent
  by Bob.
\end{lemma}

This lemma is trivial to prove since the result immediately follows from the 
definition of the problem.

\begin{lemma}
  \label{lem:limit}
If there is a constant number of rounds (a [$O(1)$, $a$, $b$] protocol),
there exists a protocol with parameters $s=2^{O(a)}$, $t=O(1)$, $b$.
\end{lemma}

proof: let us consider the vector $v=(\alpha_1, \alpha_2, \ldots, \alpha_i)$.
Alice sends each $\alpha_k$ where each $\alpha$ is `$a$' bits.  The
number of possible combinations of sequences is $2^i$, so it takes $2^{ia}$ bits.

\begin{equation*}
s = 2^a + 2^{2a} + 2^{3a} + \ldots + 2^{ta} = O(2^a)
\end{equation*}

\begin{theorem}
  \label{thm:limit}
If we have a function $f: \{0, 1\}^n \times \{0,1\}^m \rightarrow \{0,1\}$
where $n<m$, that can be computed by a polynomial size, read $O(1)$ branching program,
there exists a data structure which stores $y\in\{0,1\}^m$ can can answer any
query with the parameters $s=m^{O(1)}$ cells, $b \ge \log m$,
\mbox{$t = O(n/(\log b - \log \log m))$.}
\end{theorem}

Why are these lemmas and theorem interesting?  Lemma \ref{lem:bounds} says we can
get bounds through conversion.  Lemma \ref{lem:limit} describes how this procedure
only works up to a point.  This theorem describes why we can't do any better; because
it will solve hard problems which have not been solved.

\subsection{Richness Lemma}

The richness lemma is useful for proving lower bounds for asymmetric communication
complexity.  This will help build our tool box which we can use to prove data
structure lower bounds using asymmetric communication.

A $(y,v)$-rich problem is where at least $v$ columns contain at least a $u$
$1$-entries.

\begin{lemma}
  \label{lem:rich}
  Let $f$ be a $(u,v)$-rich problem.  $f$ has a randomized one-sided error
  [$a,b$]-protocol.
\end{lemma}

We should relate this lemma to monochromatic rectangles in solution
matrices.  However, since the matrix is no longer square, we have
to modify how we are reasoning about the problem.

\begin{lemma}
  Let $\alpha, \epsilon > V$
  
  Let $f: X \times Y \rightarrow \{0,1\}$ be an $\alpha$-dense problem, there
  exists a sub matrix $M$ is $\frac{|X|}{2^{O(a)}} \times \frac{|Y|}{2^{O(a+b)}}$.
  This implies $0$-entries in $M$ are are most $\epsilon$.
\end{lemma}

The paper used as reference is useful for creating a new set of tools which
can be used to prove bounds on data structures using asymmetric
communication complexity.

\bibliographystyle{plain}
\bibliography{296026}

\end{document}
