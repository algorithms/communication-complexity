\documentclass{tufte-handout} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{geometry} % to change the page dimensions

\usepackage{graphicx} 
\usepackage{amsmath,amsthm,url}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim

\usepackage[draft,inline,nomargin,index]{fixme}
\fxsetup{theme=colorsig,mode=multiuser,inlineface=\sffamily,envface=\itshape}
\FXRegisterAuthor{su}{asu}{Suresh}
\FXRegisterAuthor{ro}{aro}{Robert}


\newcommand{\determin}[1]{\ensuremath{\mathcal{D}\left(#1\right)}}
\newcommand{\randR}[1]{\ensuremath{\mathcal{R}\!\left(#1\right)}}
\newcommand{\equ}{\ensuremath{\mathcal{E\!Q}}}
\newcommand{\gtfunc}{\ensuremath{\mathcal{GT}}}
\newcommand{\disjoint}{\ensuremath{\mathcal{DISJ}}}
\newcommand{\mP}{\ensuremath{\mathcal{P}}}
\newcommand{\nexists}{\ensuremath{\not{\exists}}}
\newcommand{\disc}{\ensuremath{\text{\em Disc}_\mu}}
%\newcommand{\Pr}{\ensuremath{\text{Pr}}}
%%% END Article customizations

%%% The "real" document content comes below...

\title{CS 7931: Lecture 5}
\author{Robert Christensen}

\begin{document}
\maketitle

\section{More about Discrepincy}

When we consider deterministic protocols, we look at a matrix of solutions.
The leafs of the matrix determines the reported solution.  For deterministic
algorithms, all rectangles \emph{must} be monochromatic.  Randomized algorithms
can have some small errors (they can be `mostly' monochromatic).  How many
errors are allowed in a solution rectangle is measured by the discrepancy value:

\begin{equation*}
\disc(R, f) = \left( \Pr{}_{\!\mu} \left( f(x,y) = 0 \right) - \Pr{}_{\!\mu}\left( f(x,y) = 1\right) \text{ and } (x,y) \in R \right)
\end{equation*}

Basically, this is defined as the difference between the number of $1$'s and $0$'s.
If the number of $1$'s and $0$'s are nearly identical the discrepancy is small.
If the rectangle is monochromatic the discrepancy is large.  Because of this,
we define

\begin{equation*}
\disc(f) = \max_R \disc(R_0f)
\end{equation*}

With this definition, we we make the following claim:

\begin{theorem}

\begin{equation}
\label{equ:determinDisc}
\mathcal{D}_{\frac{1}{2} - \epsilon}^\mu (f) \ge \log{\left(\frac{2 \epsilon}{\disc(t)}\right)}
\end{equation}

\end{theorem}

Rectangles with large discrepancy means the bound is not very tight because
there are more things which could be done to improve the bounds.  If more
mistakes were allowed the rectangles would be larger.

To prove this, lets assume some protocol $\Pi$ which uses $c$ bits of complexity.

\begin{equation*}
\underbrace{\Pr{}_{\!\mu}\left( f(x,y) = \Pi(x,y)\right)}
   _{\text{probability that it is correct } > \frac{1}{2} + \epsilon}
- \underbrace{\Pr{}_{\!\mu}\left( f(x,y) = \Pi(x,y)\right)}
   _{\text{probability that is not correct } \le \frac{1}{2} - \epsilon} \ge 2 \epsilon
\end{equation*}

We rewrite this as a sum over all leafs.

\begin{equation*}
\label{equ:probSum}
\sum_\ell \left|
  \Pr(f(x,y)=0 \text{ and } x,y \in R_\ell) 
- \Pr(f(x,y) = 1 \text{ and } x,y \in R_\ell
  \right|
\end{equation*}

We know the inside of the sum is the value of the discrepancy, so it
naturally follows that

\begin{align*}
2 \epsilon &\le 2^C \disc(f) \\
C &> \log{\left( \frac{2 \epsilon}{\disc(f)} \right)}
\end{align*}

which is exactly what was claimed before.  If the discrepancy is small, C must
be large.  To complete the inequality, we state that 
\hbox{C is $\mathcal{D}^{\mu}_{\frac{1}{2} - \epsilon}$.}

\section{Inner Product}

Let first assume the following is true

\begin{equation*}
\text{\em Disc}_{\text{uniform}}(IP) = 2^{-\frac{n}{2}}
\end{equation*}

Using (\ref{equ:determinDisc}) and our assumption we can make
the following statement

\begin{equation*}
\mathcal{D}_{\frac{1}{2} - \epsilon}^{\text{uniform}} \ge \log{\left(2 \epsilon 2^\frac{n}{2}\right)} = \Theta(n)
\end{equation*}

Which means a randomized algorithm for inner product will have $\Theta(n)$.

We define a matrix $H$ as

\begin{equation*}
H_{ij} = \left\{
\begin{array}{l l}
+1 & \text{if $x_i \cdot y_j = 1$} \\
-1 & \text{if $x_i \cdot y_j = 0$}
\end{array} \right.
\end{equation*}

When the matrix is defined this way $H H^\top = 2^n I$ is a property of the
matrix.  Using this property we can see that $H H^\top V = 2^n V$, so $H H^\top$
has only one eigenvalue, $2^n$.  The norm of the matrix is 
\hbox{$||H|| = 2^{\frac{n}{2}}$}.  Now we know this, we want to know what the
discrepancy is of the matrix.

\begin{align*}
\text{\em Disc}_\mu (S \times T) &= \frac{| \sum_{x,y\in S \times T}H(x,y)|} {2^{2n}} \\
   &= \frac{|1_S \cdot H \cdot 1_T|}{2^{2n}}
\end{align*}

We want to upper bound the quantity in the numerator.

\begin{align*}
|1_S \cdot H \cdot 1_T| &\le ||1_S|| \cdot ||H|| \cdot ||1_T|| \\
  &\le \sqrt{|S|} \cdot 2^\frac{n}{2} \cdot \sqrt{|T|} \\
  &\le 2^\frac{n}{2} \cdot 2^\frac{n}{2} \cdot 2^\frac{n}{2} 
\end{align*}

Since $|S| and |T| \le 2^n$.  Thus, 
$\text{\em Disc}_\mu (R, f) = \frac{1}{2^{n/2}}$ for inner product.

\section{Bounding Discrepancy}

We way we have a boolean function $f(x,y)$.  There are a possibility
of $2^{2^{2n}}$ distinct boolean functions. We take a random boolean
function $f$.  We want to show that a random boolean function can not have
large monochromatic rectangles:

\begin{equation*}
\mathcal{D}_{1/2 - \epsilon}^{\text{uniform}} \ge n~O\!\left(\log{\frac{1}{\epsilon}}\right)
\end{equation*}

or

\begin{equation*}
\mathcal{D}_{1/2 - \epsilon}^{\text{uniform}} \ge \log{\left(\frac{2 \epsilon}{\text{\em Disc}_\mu(f)}\right)}
\end{equation*}

We just need to show that $\text{\em Disc}_\mu(f) \le 2^{-n}$.  We need to look at
the expected value of a rectangle (which we expect to be $E=0$ for a random
binary rectangle).  The matrix R has

\begin{equation*}
R_{ij} = \left\{ \begin{array}{l l}
1 & \text{with probability $1/2$} \\
0 & \text{with probability $1/2$}
\end{array} \right.
\end{equation*}

The key trick which we use here is that if we have a rectangles  of size $m\times m$,
the difference between $1$'s and $0$'s will never differ more than $\sqrt{m}$.
We apply a Chernoff bound next

\begin{equation*}
\Pr{\left(D \ge \alpha \right)} \le 2 e^{\frac{-2\alpha^2}{N}} \le \delta`
\end{equation*}

This is the probability for \emph{any} rectangle.  To convert this to
apply to all rectangles we apply the following operations.

\begin{equation*}
2^{2^{(n+1)}} \delta` < \frac{1}{2} - \epsilon = \delta
\end{equation*}

\begin{equation*}
2^{2^{(n+1)}} 2 e^{\frac{-2\alpha^2}{N}} \le \frac{1}{2} - \epsilon
\end{equation*}

\end{document}
